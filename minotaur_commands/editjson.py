#!/usr/bin/python2.7

import sys,os,re,json
import click
#Verify if input file name and output filename given
if len(sys.argv)<2:
    print 'Takes exactly 2 arguments '+ str(len(sys.argv)-1)+' given'
    sys.exit()

#Comments that have to be written
comments = ['#Usage:','#In Insert mode ctrl+n to get the drop down words from the dictionary file','#Sample Input:','#TypeValue','#\tAttribute Elements array1element1 array1element2 array1element3','#\tarray2element1 array2element2 array2element3']
   


def f2json():
    #Comment the File
    data = ''
    try:
        f = open(sys.argv[1],'r')
        data = '\n\n'+f.read()
        f.close()
    except:
        print ''
    f = open(sys.argv[1],'w+')
    f.writelines(('%s\n'%c for c in comments))
    f.write(data)
    f.close()
    #Open Input File in Vim
    os.system('vim '+sys.argv[1])
    #Vim Exits

    #Checks if input file is available to open
    #Helps if User didn't not save the file and quit
    try:
        inp = open(sys.argv[1],'r')
    except:
        print 'File Not Found'
        sys.exit()

    #Read File and make the Models 
    i = 0
    models = []
    def make_dict(value):
        models[i]['attributes'].append(value)
    for line in inp:
        #Skip Comments and Empty Lines
        if  (not re.match('\#',line)) and (re.search('\w+',line)):
            #get TypeValue
            if re.match('\w',line):
                each = {}
                each['type'] = re.search('(\w+)',line).group(1)
                each['attributes'] = []
                models.append(each)
                i = len(models)-1
            else:
                #gets each attribute line as an array
                #make the dict 
                line = re.findall('(\[?\w+\]?)',line)
                make_dict(line)
    inp.close()

    #Open the output file
    #writes in Json
    #Erases old file if Exists
    f = open(sys.argv[1],'w+')
    final = {'models':models}
    f.write(json.dumps(final,indent = 4))
    f.close()

#Json To Editable File
def json2f(json):
    array = json['models']
    f = open(sys.argv[1],'w+')
    f.writelines(('%s\n'%c for c in comments))
    f.close()
    f = open(sys.argv[1],'w+')
    for i in array:
        f.write(i['type']+'\n')
        for i in i['attributes']:
            string = '\t'.join(i)
            f.write('\t'+string+'\n')
        f.write('\n\n')
    f.close()

@click.command()
@click.argument('filename')
def edit_json(filename):
    try:
        f = open(sys.argv[1],'r')
        a = json.load(f)
        f.close()
        json2f(a)
        f2json()
    except:
        f2json()

import click
import os
import subprocess

actions = ['restart', 'build']
targets = ['archive', 'autoagent', 'linker', 'tm']

myghc = ['ghc', '-outputdir=../../build/', '-threaded', '-i../', '--make']

target_file = {
    'proarchive': 'MkPersistent.hs',
    'proautoagent': 'ProAutoAgent.hs',
    'prolinker': 'ProLinker.hs',
    'protm': 'ProTM.hs',
}

cd_locations = {
    'runners': 'runners',
    'src': 'src',
    'daemons': 'src/Daemons',
    'modules': 'src/Modules',
    'schemas': 'src/Schemas',
}

@click.command()
@click.argument('action')
@click.argument('target')
def cmd(action, target):
    """Procustes helper script."""
    if action == 'restart':
        restart_action(target)
    if action == 'build':
        build_action(target)
    if action == 'cd':
        change_to(target)
    if action == 'edit' and target == 'types':
        open_types()

def open_types():
    goto_procrustes()
    os.chdir('src/Schemas')
    os.system('editjson Types.json')

def change_to(target):
    goto_procrustes()

def restart_action(target):
    """Restart a specific submodule"""
    if target != 'all':
        target = clean_target(target)
        if target == 'proarchive':
            if build_archive():
                server_restart(target)
        else:
            if build(target):
                server_restart(target)
    elif target == 'all':
        build_action('all')
        for tgt in target_file:
            server_restart(tgt)

def build_action(target):
    if target == 'all':
        for tgt in target_file:
            if tgt in ['archive', 'proarchive']:
                if build_archive():
                    click.echo("Compiling "+tgt+" successfull" )
            elif build(tgt):
                click.echo("Compiling "+tgt+" successfull" )
    else:
        if target in ['archive', 'proarchive']:
            if build_archive():
                click.echo("Compiling "+target+" successfull" )
        elif build(target):
            click.echo("Compiling "+target+" successfull" )

def build(target):
    target = clean_target(target)
    goto_procrustes()
    os.chdir('src/Daemons')
    if safe_call(myghc + [target_file[target]],'Compiling ' + target + ' failed' ):
        return True
    else:
        return False

def build_archive():
    """Rebuilds a archive submodule"""
    goto_procrustes()
    os.chdir('src/Utils')
    print "src/Utils"
    try:
        os.getcwd()
        subprocess.check_call(['./MkPersistent'])
    except OSError:
        print "oserror"
        subprocess.check_call([myghc + ['MkPersistent.hs']])
        subprocess.check_call(['./MkPersistent'])
    except subprocess.CalledProcessError:
        print "suprocess error"
        click.echo("Compiling MkPersistent failed.")
        exit()
    os.chdir('../ProjectArchive')
    try:
        subprocess.check_call(myghc + ['ProServerizer.hs'])
    except subprocess.CalledProcessError:
        click.echo("Compiling ProServerizer failed.")
        exit()
    click.echo('Building archive successful.')
    print "build archive returned success"
    return True

def server_restart(server):
    goto_procrustes()
    os.chdir('runners')
    if safe_call(['./myrestart', server], 'Restarting ' + server + ' failed'):
        click.echo(server + ' Restarted')

def clean_target(target):
    if target != 'all':
        if not target.startswith('pro'):
            target = 'pro' + target
    return target

def goto_procrustes():
    path = os.getcwd()
    # Searching backwards for procrustes
    try:
        procrustes_dir = '/'.join(path.split('/')[:path.split('/').index('procrustes')+1])
    except ValueError: # Procrustes not present in backward path. May be child of current directory
        depth_count = 0 # not an efficient method. TODO: Cleanup child navigation
        for root, dirs, files in os.walk(path):
            depth_count += 1
            if 'procrustes' in root:
                if '.git' in root.split('/'):
                    pass
                else:
                    procrustes_dir = root
                    break
            if depth_count > 100:
                click.echo("procrustes not found 100 levels deep. Exiting...")
                exit()
    os.chdir(procrustes_dir)
    click.echo(os.getcwd())
    # Im inside procrustes now
    myghc[1] = '-outputdir=' + procrustes_dir + '/build/'
    myghc[3] = '-i' + procrustes_dir + '/src/'

def safe_call(command, error_message):
    try:
        subprocess.check_call(command)
        return True
    except subprocess.CalledProcessError:
        click.echo(error_message)
        return False
    except OSError:
        click.echo("File not found " + os.getcwd() + command)
